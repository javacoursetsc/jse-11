package ru.arubtsova.tm.controller;

import ru.arubtsova.tm.api.controller.ITaskController;
import ru.arubtsova.tm.api.service.ITaskService;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList() {
        System.out.println("Task List:");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public void create() {
        System.out.println("Task Create:");
        System.out.println("Enter Task Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Task Description:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) {
            System.out.println("Task Creating Failed");
            return;
        }
    }

    @Override
    public void clear() {
        System.out.println("Task Clear:");
        taskService.clear();
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("Task Overview:");
        System.out.println("Enter Task Index:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("Search failed, please, try again");
            return;
        }
        showTask(task);
    }

    @Override
    public void showTaskById() {
        System.out.println("Task Overview:");
        System.out.println("Enter Task Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("Search failed, please, try again");
            return;
        }
        showTask(task);
    }

    @Override
    public void showTaskByName() {
        System.out.println("Task Overview:");
        System.out.println("Enter Task Name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) {
            System.out.println("Search failed, please, try again");
            return;
        }
        showTask(task);
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("Task Removal:");
        System.out.println("Enter Task Index:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.removeOneByIndex(index);
        if (task == null) {
            System.out.println("Removal failed, please, try again");
        } else {
            System.out.println("Task was successfully removed");
        }
    }

    @Override
    public void removeTaskById() {
        System.out.println("Task Removal:");
        System.out.println("Enter Task Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        if (task == null) {
            System.out.println("Removal failed, please, try again");
        } else {
            System.out.println("Task was successfully removed");
        }
    }

    @Override
    public void removeTaskByName() {
        System.out.println("Task Removal:");
        System.out.println("Enter Task Name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) {
            System.out.println("Removal failed, please, try again");
        } else {
            System.out.println("Task was successfully removed");
        }
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("Task:");
        System.out.println("Enter Task Index:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("Search failed, please, try again");
            return;
        }
        System.out.println("Enter New Task Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Task Description:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = taskService.updateTaskByIndex(index, name, description);
        if (taskUpdate == null) {
            System.out.println("Update failed, please, try again");
            return;
        } else {
            System.out.println("Task was successfully updated");
        }
    }

    @Override
    public void updateTaskById() {
        System.out.println("Task:");
        System.out.println("Enter Task Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("Search failed, please, try again");
            return;
        }
        System.out.println("Enter New Task Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Task Description:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = taskService.updateTaskById(id, name, description);
        if (taskUpdate == null) {
            System.out.println("Update failed, please, try again");
            return;
        } else {
            System.out.println("Task was successfully updated");
        }
    }

}
