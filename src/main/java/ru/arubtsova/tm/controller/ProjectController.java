package ru.arubtsova.tm.controller;

import ru.arubtsova.tm.api.controller.IProjectController;
import ru.arubtsova.tm.api.service.IProjectService;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList() {
        System.out.println("Project List:");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public void create() {
        System.out.println("Project Create:");
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Description:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(name, description);
        if (project == null) {
            System.out.println("Project Creating Failed");
            return;
        }
    }

    @Override
    public void clear() {
        System.out.println("Project Clear:");
        projectService.clear();
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("Project Overview:");
        System.out.println("Enter Project Index:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("Search failed, please, try again");
            return;
        }
        showProject(project);
    }

    @Override
    public void showProjectById() {
        System.out.println("Project Overview:");
        System.out.println("Enter Project Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("Search failed, please, try again");
            return;
        }
        showProject(project);
    }

    @Override
    public void showProjectByName() {
        System.out.println("Project Overview:");
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) {
            System.out.println("Search failed, please, try again");
            return;
        }
        showProject(project);
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("Project Removal:");
        System.out.println("Enter Project Index:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.removeOneByIndex(index);
        if (project == null) {
            System.out.println("Removal failed, please, try again");
        } else {
            System.out.println("Project was successfully removed");
        }
    }

    @Override
    public void removeProjectById() {
        System.out.println("Project Removal:");
        System.out.println("Enter Project Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(id);
        if (project == null) {
            System.out.println("Removal failed, please, try again");
        } else {
            System.out.println("Project was successfully removed");
        }
    }

    @Override
    public void removeProjectByName() {
        System.out.println("Project Removal:");
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(name);
        if (project == null) {
            System.out.println("Removal failed, please, try again");
        } else {
            System.out.println("Project was successfully removed");
        }
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("Project:");
        System.out.println("Enter Project Index:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("Search failed, please, try again");
            return;
        }
        System.out.println("Enter New Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Project Description:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectByIndex(index, name, description);
        if (projectUpdate == null) {
            System.out.println("Update failed, please, try again");
            return;
        } else {
            System.out.println("Project was successfully updated");
        }
    }

    @Override
    public void updateProjectById() {
        System.out.println("Project:");
        System.out.println("Enter Project Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("Search failed, please, try again");
            return;
        }
        System.out.println("Enter New Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Project Description:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectById(id, name, description);
        if (projectUpdate == null) {
            System.out.println("Update failed, please, try again");
            return;
        } else {
            System.out.println("Project was successfully updated");
        }
    }

}
