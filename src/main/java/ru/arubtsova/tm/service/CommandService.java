package ru.arubtsova.tm.service;

import ru.arubtsova.tm.api.repository.ICommandRepository;
import ru.arubtsova.tm.api.service.ICommandService;
import ru.arubtsova.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
