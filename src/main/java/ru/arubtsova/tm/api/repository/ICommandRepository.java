package ru.arubtsova.tm.api.repository;

import ru.arubtsova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
