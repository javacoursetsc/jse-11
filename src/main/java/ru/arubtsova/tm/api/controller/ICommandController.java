package ru.arubtsova.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showVersion();

    void showSystemInfo();

    void showCommands();

    void showArguments();

    void showHelp();

    void exit();

}
